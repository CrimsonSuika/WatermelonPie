package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionCreator {
	
	private static final String url = "jdbc:postgresql://localhost/WatermelonPie";
	private static final String user = "postgres";
	private static final String password = "watermelon";
	
	private static Connection connection = null;
	
	private ConnectionCreator() {
		
	}
	
	/**
	 * Checks if the program is connected to database or not 
	 * @return Connection object
	 */
	public static Connection getConnection() throws SQLException{
		
			connection = DriverManager.getConnection(url, user, password);
			System.out.println("Connection successful!");
		
		return connection;
	}
	
}
