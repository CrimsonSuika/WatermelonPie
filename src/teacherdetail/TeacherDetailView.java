package teacherdetail;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class TeacherDetailView extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldTeacherID;
	private JTextField textFieldFirstName;
	private JTextField textFieldLastName;
	private JTextField textFieldAge;
	private JTextField textFieldTeacherCity;
	private JTextField textFieldTeacherState;
	private JButton btnNext;
	
	/**
	 * Create the frame.
	 */
	public TeacherDetailView() {
		initializeViews();
	}
	
	public void initializeViews() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panelPersonalDetails = new JPanel();
		tabbedPane.addTab("Personal Details", null, panelPersonalDetails, null);
		
		JLabel lblFirstName = new JLabel("First Name");
		
		JLabel lblLastName = new JLabel("Last Name");
		
		JLabel lblTeacherAge = new JLabel("Age");
		
		JLabel lblTeacherID = new JLabel("Teacher ID");
		
		JLabel lblTeacherCity = new JLabel("City ");
		
		JLabel lblTeacherState = new JLabel("State ");
		
		textFieldTeacherID = new JTextField();
		textFieldTeacherID.setColumns(10);
		
		textFieldFirstName = new JTextField();
		textFieldFirstName.setColumns(10);
		
		textFieldLastName = new JTextField();
		textFieldLastName.setColumns(10);
		
		textFieldAge = new JTextField();
		textFieldAge.setColumns(10);
		
		textFieldTeacherCity = new JTextField();
		textFieldTeacherCity.setColumns(10);
		
		textFieldTeacherState = new JTextField();
		textFieldTeacherState.setColumns(10);
		
		btnNext = new JButton("Next");
		GroupLayout gl_panelPersonalDetails = new GroupLayout(panelPersonalDetails);
		gl_panelPersonalDetails.setHorizontalGroup(
			gl_panelPersonalDetails.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelPersonalDetails.createSequentialGroup()
					.addGap(48)
					.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnNext)
						.addGroup(gl_panelPersonalDetails.createSequentialGroup()
							.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblTeacherID, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblFirstName, GroupLayout.DEFAULT_SIZE, 93, Short.MAX_VALUE)
								.addComponent(lblLastName, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblTeacherAge, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblTeacherCity, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblTeacherState, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(162)
							.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.LEADING, false)
								.addComponent(textFieldTeacherID, GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
								.addComponent(textFieldFirstName)
								.addComponent(textFieldLastName)
								.addComponent(textFieldAge)
								.addComponent(textFieldTeacherCity)
								.addComponent(textFieldTeacherState))))
					.addContainerGap(41, Short.MAX_VALUE))
		);
		gl_panelPersonalDetails.setVerticalGroup(
			gl_panelPersonalDetails.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panelPersonalDetails.createSequentialGroup()
					.addContainerGap(52, Short.MAX_VALUE)
					.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTeacherID)
						.addComponent(textFieldTeacherID, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblFirstName)
						.addComponent(textFieldFirstName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLastName)
						.addComponent(textFieldLastName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTeacherAge)
						.addComponent(textFieldAge, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTeacherCity)
						.addComponent(textFieldTeacherCity, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panelPersonalDetails.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTeacherState)
						.addComponent(textFieldTeacherState, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(43)
					.addComponent(btnNext)
					.addGap(31))
		);
		panelPersonalDetails.setLayout(gl_panelPersonalDetails);
		
		JPanel panelProfessionalDetails = new JPanel();
		tabbedPane.addTab("Professional Details", null, panelProfessionalDetails, null);
	}

	
	// getters and setters
	public JTextField getTextFieldTeacherID() {
		return textFieldTeacherID;
	}

	public void setTextFieldTeacherID(JTextField textFieldTeacherID) {
		this.textFieldTeacherID = textFieldTeacherID;
	}

	public JTextField getTextFieldFirstName() {
		return textFieldFirstName;
	}

	public void setTextFieldFirstName(JTextField textFieldFirstName) {
		this.textFieldFirstName = textFieldFirstName;
	}

	public JTextField getTextFieldLastName() {
		return textFieldLastName;
	}

	public void setTextFieldLastName(JTextField textFieldLastName) {
		this.textFieldLastName = textFieldLastName;
	}

	public JTextField getTextFieldAge() {
		return textFieldAge;
	}

	public void setTextFieldAge(JTextField textFieldAge) {
		this.textFieldAge = textFieldAge;
	}

	public JTextField getTextFieldTeacherCity() {
		return textFieldTeacherCity;
	}

	public void setTextFieldTeacherCity(JTextField textFieldTeacherCity) {
		this.textFieldTeacherCity = textFieldTeacherCity;
	}

	public JTextField getTextFieldTeacherState() {
		return textFieldTeacherState;
	}

	public void setTextFieldTeacherState(JTextField textFieldTeacherState) {
		this.textFieldTeacherState = textFieldTeacherState;
	}

}
