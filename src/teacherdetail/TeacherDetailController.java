package teacherdetail;

import java.sql.Connection;
import java.sql.SQLException;

import connection.ConnectionCreator;
import model.Teacher;

/**
 * 
 * @author Abhilash G
 *
 */
public class TeacherDetailController {
	
	private TeacherDetailView view;
	private Teacher model;
	private Connection connection; 
	
	public TeacherDetailController(Teacher model, TeacherDetailView view) {
		this.model = model;
		this.view = view;
		view.setVisible(true);
		try {
			connection = ConnectionCreator.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
}
